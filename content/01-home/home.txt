Title: SHOPDROPPING.NET

----

Text: 

Shopdrop: To covertly place objects* on display in a store. A form of "culture jamming" s. reverse shoplift, droplift. **

As coined and defined by (link: http://propercursive.com text: Ryan Watkins-Hughes) in 2004.


<p class="footnote">
* "Merchandise" are products of commerce; because shopdropping is capable of being unrelated to commercial aims the use of the more neutral "objects" is more appropriate. (RWH - April 14, 2010)
</p>

<p class="footnote">
** Due to the broadening usage of 'shopdropping' as a term, I have removed the sentence "Primarily used in tactical media projects and art installations." from the definition. The increased awareness of the term "culture jamming" also makes the removed sentence redundant. The synonym "droplift" comes from The Droplift Project as linked to in the Related Projects page on this site. (RWH - Dec. 26, 2007)
</p>