$(document).ready(function() {
	var current_offset = 0, limit = 10,
    load_incrementally = function() {
      $('#posts_container').tumblr({ hostname: 'shopdropping.tumblr.com', append: true, options:{start:current_offset, num: limit} });
      current_offset=current_offset+limit;
    }
  $(function() {
      load_incrementally();
      $('a.more').click(load_incrementally);
  });
  $(function () {
	  $("#slider").responsiveSlides({
	    auto: false,
	    pager: true,
	    nav: true,
	    speed: 500,
	    namespace: "centered-btns"
	  });
  });
});