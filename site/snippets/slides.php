<?php echo js('assets/js/responsive.slides.min.js') ?>
<script>
    $(function () {
      $("#slider").responsiveSlides({
        auto: false,
        pager: true,
        nav: true,
        speed: 500,
        namespace: "centered-btns"
      });
    });
 </script>