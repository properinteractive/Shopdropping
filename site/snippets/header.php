<!DOCTYPE html>
<html lang="en">
<head> 
  <title><?php echo html($site->title()) ?> - <?php echo html($page->title()) ?></title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="HandheldFriendly" content="true">
  <meta name="description" content="<?php echo html($site->description()) ?>" />
  <meta name="keywords" content="<?php echo html($site->keywords()) ?>" />
  <link rel="canonical" href="http://shopdropping.net/" />
  <?php echo css('http://fonts.googleapis.com/css?family=PT+Sans:400italic,400,700,700italic') ?>
  <?php echo css('assets/styles/styles.css') ?>
</head>
<body>
<div class="menu">
  <?php snippet('menu') ?>
</div>