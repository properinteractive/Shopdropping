<?php echo js('assets/js/jquery.tumblr.js') ?>
<script language="javascript">
    var current_offset = 0, limit = 10,
    load_incrementally = function() {
      $('#posts_container').tumblr({ hostname: 'shopdropping.tumblr.com', append: true, options:{start:current_offset, num: limit} });
      current_offset=current_offset+limit;
    }
    $(document).ready(function() {
      load_incrementally();
      $('a.more').click(load_incrementally);
    });
</script>