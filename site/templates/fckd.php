<?php snippet('header') ?>
<div class="content">
	<?php echo kirbytext($page->text()) ?>
	<div class="rslides_container">
      <ul class="rslides" id="slider">
        <li>
          <img src="http://lh6.googleusercontent.com/-O0f9Dg-TqPg/S-eOcLfCQpI/AAAAAAAAAQo/eqhPKsGpVYo/s720/fckdDROP01.jpg" />
      	</li>
      	<li>
          <img src="http://lh6.googleusercontent.com/-lgJiIeYlXSU/S-ePaNqiH5I/AAAAAAAAAQw/C3helVN4yG4/s912/fckdMAG01.jpg" />
      	</li>
        <!--  <li>
          <img src="https://lh5.googleusercontent.com/-QYWAQVf31vQ/S-ePa2ayPvI/AAAAAAAAAQ4/mD_hRf_EiV4/s725/fckdMAG02.jpg" />
        </li>
        <li>
          <img src="https://lh5.googleusercontent.com/-3wF22F3gjCo/S-ePbZ_gE6I/AAAAAAAAARE/hp6v8UEkt5g/s912/fckdMAG03.jpg" />
        </li>
        <li>
          <img src="https://lh4.googleusercontent.com/-_bTSEk3UN_8/S-ePccIbL7I/AAAAAAAAARo/7nM2traEZ-o/s500/fckdMAG04.jpg" />
        </li> -->
        <li>
          <img src="https://lh4.googleusercontent.com/-T_U1D2gb8wo/S-ePeUMbR9I/AAAAAAAAAQI/OSR2VAXsen0/s720/fckdMAG05.jpg" />
        </li> 
        <li>
          <img src="https://lh6.googleusercontent.com/-QJ7Q347jFrk/S-eOgIASApI/AAAAAAAAAQg/VcYsQ9NPJSk/s640/fckdDROP05a.JPG" />
        </li>
        <li>
          <img src="https://lh5.googleusercontent.com/-9PfinRqvdi0/S-ePfYgcyuI/AAAAAAAAARA/99Hp4yhpfUA/s720/fckdMAG06.jpg" />
        </li>
        <li>
          <img src="https://lh3.googleusercontent.com/-V94mmfDeNTc/S-eOwFx3W-I/AAAAAAAAASU/_2g9N4hidl4/s640/fckdDROP06d.JPG" />
        </li>
        <li>
          <img src="https://lh3.googleusercontent.com/-ksTYUslSB5U/S-ePh2NAcCI/AAAAAAAAAQQ/reRCD5VX7WQ/s800/fckdMAG07.jpg" />
        </li>
        <li>
          <img src="https://lh5.googleusercontent.com/-Hve1T_UUc4k/S-eOyFPp8zI/AAAAAAAAASc/9chNmD5exFY/s640/fckdDROP07a.JPG" />
        </li>
        <!-- <li>
          <img src="https://lh4.googleusercontent.com/-zUhc4dEtUZ8/S-ePmdSTuhI/AAAAAAAAAP4/jH47USym6Cc/s512/fckdMAG08.jpg" />
        </li> -->
        <li>
          <img src="https://lh4.googleusercontent.com/-oge0iYCgDUQ/S-eO3TQ7y-I/AAAAAAAAATQ/7Te_ndBtt2E/s640/fckdDROP08b.JPG" />
        </li>
        <!-- <li>
          <img src="https://lh4.googleusercontent.com/-Q7TQfZlKGtY/S-ePiz4xvbI/AAAAAAAAAP0/pyRZ-fZlBZY/s512/fckdMAG09.jpg" />
        </li> -->
        <li>
          <img src="https://lh5.googleusercontent.com/-cF8CLHkciTI/S-ePBMbNcMI/AAAAAAAAASs/mcATwwtgMUU/s640/fckdDROP09a.JPG" />
        </li>
        <!-- <li>
          <img src="https://lh6.googleusercontent.com/-nQrFJjLKrkk/S-ePoP4WpiI/AAAAAAAAAQA/E3ZspAtCgxI/s512/fckdMAG10.jpg" />
        </li> -->
        <li>
          <img src="https://lh3.googleusercontent.com/-U5ASB8CQPNc/S-ePQnUpTKI/AAAAAAAAAUk/CPhHF5WgYQg/s640/fckdDROP10b.JPG" />
        </li>
        <!-- <li>
          <img src="https://lh6.googleusercontent.com/-znE3aWcdPyI/S-ePljyQ1fI/AAAAAAAAAQY/p5QU4jPnggs/s512/fckdMAG11.jpg" />
        </li> -->
      </ul>
	</div>
</div>
<?php echo js('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js') ?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
window.jQuery || document.write("<script src='assets/js/jquery.min.js'>\x3C/script>")
//--><!]]>
</script>
<?php snippet('slides') ?>
<?php snippet('footer') ?>