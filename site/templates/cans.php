<?php snippet('header') ?>
<div class="content">
	<?php echo kirbytext($page->text()) ?>
	<div class="rslides_container">
      <ul class="rslides" id="slider">
        <li>
          <img src="http://lh4.googleusercontent.com/-e3ZGHyLI9h0/UQ7ljRLlX7I/AAAAAAAABMA/G_pqCaMxlVA/s775/2005_Brussels.jpg" /></li>
        <li>
          <img src="http://lh5.googleusercontent.com/-MZ5O5uNJMtI/UQ7ljtffncI/AAAAAAAABMI/v7_lIS6BFMg/s1143/2005_Brussels2.jpg" /></li>
        <li>
          <img src="http://lh6.googleusercontent.com/-NYCXKpUXcZk/UQ7jqzsMfEI/AAAAAAAABLY/N19UPU4SyYQ/s800/shopdropping03.jpg" />
        </li> 
        <li>
          <img src="http://lh3.googleusercontent.com/-JNz-VbFmExE/UQ7jpMDE0BI/AAAAAAAABLA/AVqXRXNbpIQ/s800/shopdropping04.jpg" />
        </li>
        <li>
          <img src="http://lh3.googleusercontent.com/-JiSGBZtYraI/UQ7loHkbCYI/AAAAAAAABMY/B4wB8RkSRsc/s775/2005_Providence.jpg" />
        </li>
        <li>
          <img src="http://lh3.googleusercontent.com/-71p8pWyzjvE/UQ7lov8rKwI/AAAAAAAABMc/Ys0gp8ljW2s/s775/2008_Hamburg.jpg" />
        </li>
        <li>
          <img src="http://lh6.googleusercontent.com/-uBcwo-wTHFI/UQ7loV5L8-I/AAAAAAAABMg/-IzSouQR_Jk/s775/2008_Hamburg2.jpg" />
        </li>
        <li>
          <img src="http://lh4.googleusercontent.com/-4O-zKsJ3c4E/UQ7lsHokzbI/AAAAAAAABMw/pQFg2KKitDw/s775/2008_Hamburg3.jpg" />
        </li>
      </ul>
	</div>
</div>
<?php echo js('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js') ?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
window.jQuery || document.write("<script src='assets/js/jquery.min.js'>\x3C/script>")
//--><!]]>
</script>
<?php snippet('slides') ?>
<?php snippet('footer') ?>