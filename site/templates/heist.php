<?php snippet('header') ?>
<div class="content">
	<div class="rslides_container">
      <ul class="rslides" id="slider">
        <li>
          <img src="https://lh5.googleusercontent.com/-2UNgavYHdWU/TAZnY6h3wpI/AAAAAAAAAcI/bI8TN7Hyd1w/s600/Heist_outside01.jpg" /></li>
        <li>
          <img src="https://lh4.googleusercontent.com/-BA9XZXQpsRI/TAZnZah0RqI/AAAAAAAAAcM/VNOY-44lB_k/s600/Heist_outside02.jpg" /></li>
        <li>
          <img src="https://lh4.googleusercontent.com/-bKfoneaI4Ao/TAZnZtKbOEI/AAAAAAAAAcQ/0RFHvLqG4n8/s600/Heist_outside03.jpg" />
        </li> 
        <li>
          <img src="https://lh3.googleusercontent.com/-7_nzWAdGZYs/TA76RE_QkCI/AAAAAAAAAx4/SkIwac3KVtU/s700/01.jpg" />
        </li>
        <li>
          <img src="https://lh6.googleusercontent.com/-RDWRZeg6_sE/TA76RrahOaI/AAAAAAAAAx8/hA2lLz2hLnM/s700/02.jpg" />
        </li>
        <li>
          <img src="https://lh5.googleusercontent.com/-tw2R6YFcZqA/TA76SbRJc-I/AAAAAAAAAyA/8HgxNvMH_fE/s700/03.jpg" />
        </li>
        <li>
          <img src="https://lh6.googleusercontent.com/-bV3aVUMvCcY/TA76S814caI/AAAAAAAAAyE/zo0YEB9zKao/s700/04.jpg" />
        </li>
        <li>
          <img src="https://lh4.googleusercontent.com/-WM1lUHd8OWs/TA76Tbc8TbI/AAAAAAAAAyI/oIL2Ifmncmc/s700/05.jpg" />
        </li>
      </ul>
	</div>
  <?php echo kirbytext($page->text()) ?>
</div>
<?php echo js('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js') ?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
window.jQuery || document.write("<script src='assets/js/jquery.min.js'>\x3C/script>")
//--><!]]>
</script>
<?php snippet('slides') ?>
<?php snippet('footer') ?>